// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.
// Execute a function and return an array that only contains BMW and Audi cars.
// Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

export function problem6(inventory) {
    let BMWAndAudi =["NO BMW and Audi cars in the inventory"]; //The default string will displays if there is no shotlisted cars from the inventory.
    let j=0;

for (let i = 0; i < inventory.length; i++) {
    if(inventory[i].car_make == "BMW" || inventory[i].car_make == "Audi"){
        BMWAndAudi[j++]="Car " + inventory[i].id + " is a " + inventory[i].car_year + " " + inventory[i].car_make + " " + inventory[i].car_model;
    }
}

const myJSON = JSON.stringify(BMWAndAudi);

return myJSON;
}