// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot.
// Execute a function that will return an array from the dealer data 
// containing only the car years and log the result in the console as it was returned.


export function problem4(inventory) {
    let car_array=[],count = 0;
    for (let i = 0; i < inventory.length; i++) {
        car_array[count++] = inventory[i].car_year; 
    }
    return car_array;
}