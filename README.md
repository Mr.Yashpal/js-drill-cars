# JS Drill: Cars Submission.

### In this,
* I solve 6 problems (name: problem1, problem2 and so on and so forth)

* All files in root directory with a **test** folder.

* 6 more files for testing all those root files.

* And an inventory file for storing all **Lot** data (For easy **update** and use).

* And there is a **pacage.json** file for using import and export functionality.

You can see all the problems and details [here.](https://learn.mountblue.io/targets/2151)(Need credentials to see).