// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
// Using the array you just obtained from the previous problem, find out how many cars
// were made before the year 2000 and return the array of older cars and log its length.

var old_cars = [],old_cars_length = 0;

export function problem5(car_array) {
    for (let i = 0; i < car_array.length; i++) {
        if(car_array[i] < 2000){
            old_cars[old_cars_length] = car_array[i];
            old_cars_length++;
        }
    }
    return (old_cars);
}