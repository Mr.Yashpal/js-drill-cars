import {problem4} from '../problem4.js'
import {problem5} from '../problem5.js'
import {inventory} from './inventory.js'

// let older_cars = [];

let expected_count = 25,expected_solution = [
  1983, 1990, 1995, 1987, 1996,
  1997, 1999, 1987, 1995, 1994,
  1985, 1997, 1992, 1993, 1964,
  1999, 1991, 1997, 1992, 1998,
  1965, 1996, 1995, 1996, 1999
];
let older_cars = problem5(problem4(inventory));

if( expected_count == expected_solution.length){
    for(let i=0;i<expected_solution.length;i++){
        if (expected_solution[i] == older_cars[i]){
        if(i == expected_solution.length-1)
        console.log("Test Passed");
        }
        else{
            console.log("Test Failed: car year don't match");
            break;
        }
    }
}
else
    console.log("Test Failed: car count don't match");