import {problem3} from '../problem3.js'
import {inventory} from './inventory.js'

let expected_solution = ['300M','4000CS Quattro','525','6 Series','Accord','Aerio','Bravada','Camry','Cavalier','Ciera','Defender Ice Edition','E-Class','Econoline E250','Escalade','Escort','Esprit','Evora','Express 1500','Familia','Fortwo','G35','Galant','GTO','Intrepid','Jetta','LSS','Magnum','Miata MX-5','Montero Sport','MR2','Mustang','Navigator','Prizm','Q','Q7','R-Class','Ram Van 1500','Ram Van 3500','riolet','Sebring','Skylark','Talon','Topaz','Town Car','TT','Windstar','Wrangler','Wrangler','XC70','Yukon'];

let result = problem3(inventory);

for(let i=0;i<expected_solution.length;i++){
    if (expected_solution[i] == result[i]){
        if(i == expected_solution.length-1)
        console.log("Test Passed");
    }
    else{
        console.log("Test Failed");
        break;
    }
}