import {problem1} from '../problem1.js'
import {inventory} from './inventory.js'

let result = problem1(inventory);

if(result == "Car 33 is a 2011 Jeep Wrangler")
    console.log("Test passed");
else
    console.log("Test Failed");