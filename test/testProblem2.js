import {problem2} from '../problem2.js'
import {inventory} from './inventory.js'


let result = problem2(inventory);

if(result == "Last car is a Lincoln Town Car")
    console.log("Test passed");
else
    console.log("Test Failed");